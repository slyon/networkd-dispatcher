Source: networkd-dispatcher
Section: utils
Priority: optional
Maintainer: Julian Andres Klode <jak@debian.org>
Rules-Requires-Root: no
Build-Depends: asciidoc,
               debhelper-compat (= 13),
               dh-python,
               docbook-xml,
               docbook-xsl,
               gir1.2-glib-2.0,
               iw | wireless-tools,
               python3-dbus,
               python3-gi,
               python3-mock,
               python3-pytest,
               python3:any,
               xsltproc
Standards-Version: 4.5.0
Homepage: https://gitlab.com/craftyguy/networkd-dispatcher
Vcs-Git: https://salsa.debian.org/debian/networkd-dispatcher.git
Vcs-Browser: https://salsa.debian.org/debian/networkd-dispatcher

Package: networkd-dispatcher
Architecture: all
Depends: dbus,
         gir1.2-glib-2.0,
         python3-dbus,
         python3-gi,
         ${misc:Depends},
         ${python3:Depends}
Suggests: iw | wireless-tools
Description: Dispatcher service for systemd-networkd connection status changes
 Networkd-dispatcher is a dispatcher daemon for systemd-networkd
 connection status changes. It is similar to NetworkManager-dispatcher,
 but is much more limited in the types of events it supports due to the
 limited nature of systemd-networkd.
